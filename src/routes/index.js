import React from 'react'
import {createAppContainer,createSwitchNavigator} from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';

import introScreen from "../screens/introScreen";
import wlcScreen from "../screens/wlcScreen";
import hofListScreen from '../screens/hofListScreen';

const screensRoutes = createStackNavigator({

    welcomeScreen :{
        screen : wlcScreen,
        navigationOptions: {
            header: null
        },
    },
    listScreen:{screen:hofListScreen}


}, {
    initialRouteName: "welcomeScreen",

});


const appRoutes = createSwitchNavigator({
    introScreen:introScreen,
    screensRoutes:screensRoutes
}, {
    initialRouteName: 'introScreen'
});


export default createAppContainer(appRoutes);
