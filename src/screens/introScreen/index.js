import React, { useState, useEffect} from 'react';
import { Text, View } from 'react-native';
import AppIntroSlider from 'react-native-app-intro-slider';
import renderItem from "../../layouts/slider/renderItem";
import { useNavigation } from 'react-navigation-hooks';
import storageHelper from '../../helpers/storageHelper';
import styles from './index.style';
 
const slidesArray = [
    {
      key: 'somethun',
      title: 'Title 1',
      text: 'Description.\nSay something cool',
      image: require('../../../assets/1.gif'),
      backgroundColor: '#59b2ab',
    },
    {
      key: 'somethun-dos',
      title: 'Title 2',
      text: 'Other cool stuff',
      image: require('../../../assets/2.gif'),
      backgroundColor: '#febe29',
    },
    {
      key: 'somethun1',
      title: 'Rocket guy',
      text: 'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
      image: require('../../../assets/3.gif'),
      backgroundColor: '#22bcb5',
    }
  ];


export default function introScreen() {
      const { navigate } = useNavigation();
      const [state, setState] = useState({slides:slidesArray, loading: true});
  
      async function isDone (){
       await storageHelper.setAsyncStorage(storageHelper.OpenApp, 'true');
       navigate('welcomeScreen');
    };

        async function getIsOpenApp() {
            let openApp = await storageHelper.getAsyncStorage(storageHelper.OpenApp);
            if (openApp){
                navigate('welcomeScreen');
            }else {
                setState({...state , loading:false});
            }
        }

      useEffect(() => {
        getIsOpenApp();
      },[]);


    const { slides, loading } = state;

    return (
        loading ?
        <View style={styles.content}>
            <Text style={styles.text}>loading...</Text>
        </View>: <AppIntroSlider renderItem={renderItem} slides={slides} bottomButton onDone={isDone}/>
        );    
}
