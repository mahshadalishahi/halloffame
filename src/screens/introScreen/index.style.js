import { StyleSheet } from 'react-native'

export default StyleSheet.create({

    content:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
        backgroundColor: '#E4FA8E'
    },

    text:{
        textAlign: 'center',
        fontSize:16
    },



})
