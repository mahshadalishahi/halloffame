import React, { useState, useEffect } from 'react';
import { Text, View, Button, Image, TextInput, Alert} from 'react-native';
import styles from './index.style';
import { useNavigation } from 'react-navigation-hooks';

const shuffleSeed = require('shuffle-seed');

const gifsArray = [{
        image:require('../../../assets/giphy1.gif'),
        backgroundColor: '#E4FA8E'
    },{
        image:require('../../../assets/giphy2.gif'),
        backgroundColor: '#FE2CFF'
    },{
        image:require('../../../assets/giphy3.gif'),
        backgroundColor: '#83F3F7'
    },{
        image:require('../../../assets/giphy4.gif'),
        backgroundColor: '#FF7886'
    },{
        image:require('../../../assets/giphy5.gif'),
        backgroundColor: '#FE2CFF'
    }];

export default function wlcScreen() {
    const [state, setState] = useState({gifs: gifsArray, count : 0, text: 1 });
    const { gifs, count, text } = state;
    const { navigate } = useNavigation();

    function Separator() {
        return <View style={styles.separator} />;
      }

    useEffect(() => {
        let showGifs= setInterval(() => {
            setState(prevState=> ({...prevState, count: prevState.count + 1 !== gifs.length ? prevState.count + 1 :  0 }))
        }, 500);

        return ()=> clearInterval(showGifs);
      },[]);

      function save () {
        if (!text) {
            Alert.alert('value not valide!');
            return;
        }else if(parseInt(text) < 1 || parseInt(text) > 5){
             Alert.alert('value not valide!');
             return;
        }

        setState(prevState=> ({...prevState, gifs: gifsArray, count: parseInt(text)- 1 }))
      }
   
      function randomise () {
        let num = Math.floor(Math.random() * 9) + 1;
        let newArray = shuffleSeed.shuffle(gifs, num);
        setState(prevState=> ({...prevState, gifs: newArray, count: 0 }))

      }

    return (
        <View style={[styles.content,{backgroundColor:gifs[count].backgroundColor}]}>
            <Text style={styles.text}>Welcome To Hall Of Fam </Text>
            <Image source={gifs[count].image} style={{width: 200, height: 150, marginTop: 10 }}/>
            <View>
            <TextInput style={styles.input} defaultValue="1" keyboardType={'numeric'}
                      onChangeText={(text) => setState(prevState=> ({...prevState, text: text}))} />
            <Button 
                title="save" 
                color="#f194ff" 
                onPress={save}/>
         
            <Separator/>
           <Button 
                title="Randomise"
                onPress={randomise}/>
            <Separator/>
            <Button 
                title="hall of fame"
                onPress={()=>navigate('listScreen')}/>
            </View>
          
        </View>
    );
}
