import { StyleSheet } from 'react-native'

export default StyleSheet.create({

    content:{
        flex:1,
        alignItems:'center',
        justifyContent:'center',
    },

    text:{
        textAlign: 'center',
        fontSize:16
    },

    input:{
        height: 40, 
        borderColor: 'gray', 
        borderBottomWidth: 1,
        width: 250,
        marginVertical: 20
    },

    buttons:{
        marginVertical: 20,
        width: 200,
        padding:10
    },
    separator: {
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
      }


})
