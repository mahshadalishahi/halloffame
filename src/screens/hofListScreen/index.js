import React, { useState, useEffect } from 'react';
import {Text, View, Button, BackHandler, Alert} from 'react-native';
import { useNavigation } from 'react-navigation-hooks';
// const { goBack } = useNavigation();

export default function hofListScreen() {

    const [backClickCount, setBackClickCount] = useState(0);

    function backClick() {
        setBackClickCount(prevState => prevState + 1);
        Alert.alert('press back again to exit the app');
    }

    function backButtonHandler() {
        console.log('heeeee0' , backClickCount);
        backClickCount === 1 ? BackHandler.exitApp() : backClick();
        return true;
    }
    
    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", backButtonHandler);
        return () => {
          BackHandler.removeEventListener("hardwareBackPress", backButtonHandler);
        };
      }, []);

    return (
        <View>
            <Text>hof list screen . third screen</Text>
        </View>
    );
}+

// hofListScreen.navigationOptions = {
//     headerTitle: "Hall Of Fame",
//     headerRight: () => (
//       <Button
//         onPress={goBack}
//         title=">"
//         color="#fff"/>
//     ),
//     };
