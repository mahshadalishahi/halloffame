import { StyleSheet } from 'react-native'

export default StyleSheet.create({

    slide:{
        flex:1,
        alignItems:'center',
        justifyContent:'center'

    },
    text:{
        textAlign: 'center',
        color:'#ffffff',
        marginVertical: 50
    },
    title:{
        textAlign: 'center',
        color:'#ffffff',
        marginVertical: 50
    },


})
