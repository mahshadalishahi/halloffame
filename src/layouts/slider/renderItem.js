

import React from 'react';
import { Text, View, Image } from 'react-native';
import styles from './index.style';

export default function renderItem({item}) {

    return (
        <View style={[styles.slide,{backgroundColor:item.backgroundColor}]}>
            <Text style={styles.title}>{item.title}</Text>
            <Image source={item.image} style={{width: 300, height: 300 }}/>
            <Text style={styles.text}>{item.text}</Text>
        </View>
    );
}
